# Monthly_Backup_Archive
# Version: 1.1

. ('S:\Statistics\10K\Programs\Scripts\tools.ps1') 								# Include Tools (Our custom functions)
$dir = PackagerStart															# This script is designed to be run from the archive directory
Write-Output "Monthly Archiver v1.1"

#List all Server backup directories here!
#===============================================================================
[array]$Servers = @(
	@{
		Alias =
			"DC00SQL01";
		Path = 
			"\\dc00nas01\backups\DC00SQL01";
	},
	@{
		Alias =
			"DC00SQL02";
		Path = 
			"\\dc00nas01\backups\DC00SQL02";
	}
);
#===============================================================================

[System.Collections.ArrayList]$copySteps1 = @()									# Create empty ArrayList (Non-fixed size, good for our purposes)
$Pattern = "(\d{4}_\d{2}_\d{2})"												# RegEx pattern to parse Date from FileNames
$Format = "yyyy_MM_dd"															# Expected Date Format

foreach ($Server in $Servers) {													# Loop through server directories
	Write-Output "Parsing $($Server.Alias)"

	$Databases = Get-ChildItem $Server.Path | 									# Return list of folders in server directory
				 Where-Object {$_.PSIsContainer} |
				 Foreach-Object {$_}

	foreach ($Database in $Databases) {											# Loop through database directories
		$LatestBackup = 
			Get-ChildItem -Path "$($Database.fullname)\*.bak" |					# Return most recent backup file
			Sort-Object -Property @{											# This section gets weird. Sorting call happens on the following:
				Expression={
					[DateTime]::ParseExact(										# A DateTime.Day object with specific parse instructions
						[RegEx]::Match($_.Name, $Pattern).Value,				# Date pulled out of filename with RegEx pattern defined above
						$Format,												# Instructions for specific date format defined above
						$Null													# Null because I don't care what IFormatProvider is used
					).Day
				}
			} -Descending |														# Descending sort flag, to make sure we are looking at latest first
			Select-Object -First 1

		if ($LatestBackup) {													# Confirm that this $Database loop found a .bak file to copy
			[void]$copySteps1.add(												# Add each backup file to copySteps1 array for DoCopy function
				@{
					Destination=												# Built destination based on script location,
						"$dir\$($Server.Alias)\$($Database.name)\";				# Server path leaf, and database name
					Paths = $LatestBackup.fullname;								# Returns that backup file path we found above
					ExpectedDocuments = 1;
				}
			); 
		}
	}
}

Write-Output "Beginning copy operations..."
DoCopy $copySteps1																# Run steps

PackagerEnd																		# Returns us to our beginning directory
WaitForFeedBack																	# Press the any key prompt (Where's the "any" key?!)
																				# http://www.troll.me/images/home-simpson-panic/wheres-the-any-key.jpg