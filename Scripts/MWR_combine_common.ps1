##
# MWR Combiner
# Last Modified: September 15, 2014
##

Function ApplyRenameRules($rules,$name) {

	$rules | Foreach-Object {
		$name = $name -replace [regex]$_.Search, [regex]$_.Replace	
	}

	$name -replace "_"," "
}

$commonPath = Split-Path $script:MyInvocation.MyCommand.Path
. ($commonPath + '/tools.ps1')

$dir = $PWD
Write-host "Current directory is $dir"
Push-Location $dir

Write-Host -ForegroundColor Yellow "MWR Combiner"
$dateString = ""
$dateString = Read-Host 'Enter Date (YYYY-MM):' 
$dateString = $dateString.ToUpperInvariant()

if ($dateString -notmatch "\d{4}-[A-Za-z0-9]{2}") {
	Write-Host -ForegroundColor Red "Invalid Date Format"
	Exit
}

Write-Host "Cleaning up"
Write-Host "--------------------------------"
New-Item -ItemType Directory -Force -Path "Interm"
Get-ChildItem "Interm" -Recurse |  ?{ !$_.PSIsContainer } | Remove-Item –Force 


Write-Host "Running Combiner for $dateString"
Write-Host "--------------------------------"
Write-Host "Copying PDFs to Interm Folder..." -NoNewline
Copy-Item "*.pdf" "Interm\" -Exclude "*_MWR_*.*"
Write-Host "Done"

Write-Host "Removing bookmarks and counting pages..."


Push-Location "Interm"
# Generate list of files
[array]$files = @()
$bringToFront | ForEach-Object {
	$files += @(Get-ChildItem $_)
}
$files += @(Get-ChildItem "*.pdf" -Exclude $bringToFront)

"[Outlines]" | Out-File "template.ini" -Encoding ASCII

$currentPage = 1;
$x = 0;
$files  | `
Foreach-Object{	
	Write-Host "Processing: $($_.FullName)..." -NoNewline

	# Count Pages
	$arguments = @($_.FullName,"dump_data")
	$data = (pdf $arguments) | out-string
	$val = ([regex]"NumberOfPages: (\d+)").matches($data) | foreach {$_.Groups[1].Value}	
	$name =  ([regex]"(?im)InfoKey: Title\r\nInfoValue: ([^\r\n]+)").matches($data) | foreach {
		$_.Groups[1].Value
	}	
	Add-Member -InputObject $_ -Name "PageCount" -Value $val -MemberType NoteProperty	
	
	Write-Host "Done."
	
	if ($name -and $DoNotUsePDFPropertiesForTitles -ne $true) {
		$niceName = $name
	} else {
		$niceName = $_.BaseName -replace "-"," " -replace "Cover ",""
	}
	
	Add-Member -InputObject $_ -Name "Title" -Value $niceName -MemberType NoteProperty
		
	"p$($currentPage) x0 y0 >$($niceName)"  | Out-File "template.ini" -Append -Encoding ASCII
	
	$bookmarksMatches = ([regex]"BookmarkTitle: ([^\r\n]+)\r\n.*?\r\n?BookmarkPageNumber: (\d+)\r\n").matches($data)
	
	$bookmarksMatches | foreach {		
		$value = ApplyRenameRules $bookmarkRenameRules $_.Groups[1].Value
		"	p$($currentPage + $_.Groups[2].Value - 1) x0 y0 >$($value)"  | Out-File "template.ini" -Append -Encoding ASCII
	}	
	
	
	$currentPage += ($_.PageCount)	
	Write-Host "Done."
}

$files | ForEach-Object {
	Write-Host $_.Name
}
Write-Host "Merging Documents..." -NoNewline
#[array]$arguments = @("-dBATCH","-dNOPAUSE","-dQUIET","-sDEVICE=pdfwrite","-sProcessColorModel=DeviceCMYK","-sColorConversionStrategy=CMYK","-sColorConversionStrategyForImages=CMYK","-dPDFSETTINGS=/printer","-sOutputFile=FINAL_MWR_$dateString.pdf"
#	) + @($files)
[array]$arguments = @($files) + @("cat","output","FINAL_MWR_$dateString.pdf")
pdf $arguments
if ($LASTEXITCODE -gt 0) # Ignores warnings which use exit code 1.
{
	throw "PDFTK failed with exit code $LASTEXITCODE"
}
Write-Host "Done"


"[Info]
Title=""Marketwatch Report for $dateString""
Subject=""Marketwatch Report (MWR)""
Author=""10K Research and Marketing""
Keywords=""$dateString""

[XMP-Metadata]
Delete=1

[ViewerPreferences]
PageLayout=""SinglePage""
PageMode=""UseOutlines""
FirstPage=1
" | Out-File "template.ini" -Append -Encoding ASCII

Write-Host "Setting PDF Properties..." -NoNewline
[array]$arguments = @("FINAL_MWR_$dateString.pdf","-tf","template.ini","-f","-q")
pdfmeta $arguments    
if ($LASTEXITCODE -gt 0) # Ignores warnings which use exit code 1.
{
	throw "BeCyPDFMetaEdit failed with exit code $LASTEXITCODE"
}
Write-Host "Done"


# Back to main directory
Pop-Location

[array]$copySteps = @(
	@{
		Destination= 
			".";
		Paths = @(
			"Interm/FINAL_MWR_$dateString.pdf"
			);
		Exclude = @(
		);
		ExpectedDocuments = 1
	}
);

DoCopy $copySteps



Write-Host "File Order"
$files | ForEach-Object {
	Write-Host $_.Name
}



$stats = @{}
$stats.Add("Total Files",$files.Count)

Write-Host ( $stats | Out-String )
Write-Host "Press any key to exit..."
#[console]::ReadKey("NoEcho,IncludeKeyDown")






# now back to previous directory
Pop-Location