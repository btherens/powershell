# Stop on error
$ErrorActionPreference = "Stop"

# Tools definition
#$pdftk = "C:\Users\Andrei\Desktop\SCR\PDFtk\pdftk.exe" 
$pdftk = "S:\Statistics\10K\Programs\PDFtk\pdftk.exe" 
set-alias pdf $pdftk

#$ghostscript = "C:\Program Files (x86)\gs\gs9.15\bin\gswin32c.exe"
$ghostscript = "S:\Statistics\10K\Programs\GhostScript\gswin32c.exe" 
set-alias gs $ghostscript

#$pdfmeta = "C:\Users\Andrei\Desktop\SCR\BeCyPDFMetaEdit\BeCyPDFMetaEdit.com" 
$pdfmeta = "S:\Statistics\10K\Programs\BeCyPDFMetaEdit\BeCyPDFMetaEdit.exe" 
set-alias pdfmeta $pdfmeta

#$7Zip = "c:\Program Files\7-Zip\7z.exe" 
$7Zip = "S:\Statistics\10K\Programs\7-Zip\7z.exe" 
set-alias sz $7Zip

# Load WinSCP .NET assembly
#[Reflection.Assembly]::LoadFrom("\\C:\Users\Andrei\Desktop\SCR\WinSCP\WinSCPnet.dll") | Out-Null
#[Reflection.Assembly]::LoadFrom("\\S:\Statistics\10K\Programs\WinSCP\WinSCPnet.dll") | Out-Null

$toolExists = Test-Path $pdftk
if ($toolExists -eq $false) {
	Write-Error "PDFtk Cannot be found. Please install it before running this application."
	Exit
}

$toolExists = Test-Path $ghostscript
if ($toolExists -eq $false) {
	Write-Error "GhostScript Cannot be found. Please install it before running this application."
	Exit
}

$toolExists = Test-Path $pdfmeta
if ($toolExists -eq $false) {
	Write-Error "BeCyPDFMetaEdit Cannot be found. Please install it before running this application."
	Exit
}

$toolExists = Test-Path $7Zip
if ($toolExists -eq $false) {
	Write-Error "7-Zip Cannot be found. Please install it before running this application."
	Exit
}



$FTPTargets = @{
    # default FTP Target
    "10K" = @{
        Host = "143.95.152.109";
        Username = "andrei@10kresearch.com";
        Password = "WhyCanada?";
        Port = "21";
        FingerPrint = "58:01:0d:1d:88:9a:64:64:aa:f0:e2:0d:b9:71:65:29:e0:48:cf:8f"
    }
}
$FTPTargetsDefault = "10K"

Function FSTFilePath($Envirn)	{
	#Options: Prod / Staging
	# Return FST Path
	if ($Envirn -eq "Prod") {
		$FSTPath = "\\DC00WEB03.production.showingtime.com\WebsiteFiles\Prod\"
	}
	if ($Envirn -eq "Staging") {
		$FSTPath = "\\DC00WEB03.production.showingtime.com\WebsiteFiles\Staging\"
	}
	return $FSTPath
}

Function DoUpload($steps) {
    foreach ($step in $steps) {
        if ($step.Type -eq "FTP") {
            # Fixup destination just in case someone used wrong slashes

            $step.DestinationFolder = $step.DestinationFolder -replace "\\","/"

            #FTP Target
		    Write-Output "Running UPLOAD step for destination Type=$($step.Type) Target=$($step.Target) [$($step.Destination)]..."
            if ($step.Target) {
                $FTPTarget = $FTPTargets[$step.Target];
            } else {
                $FTPTarget = $FTPTargets[$FTPTargetsDefault];
            }

            #WinSCP
            # Setup session options
            $sessionOptions = New-Object WinSCP.SessionOptions
            $sessionOptions.Protocol = [WinSCP.Protocol]::Ftp
            $sessionOptions.FtpMode = [WinSCP.FtpMode]::Passive
            $sessionOptions.HostName = $FTPTarget.Host
            $sessionOptions.UserName = $FTPTarget.Username
            $sessionOptions.Password = $FTPTarget.Password
            $sessionOptions.FtpSecure = [WinSCP.FtpSecure]::ExplicitTls 
            $sessionOptions.PortNumber = 21
            $sessionOptions.TlsHostCertificateFingerprint = $FTPTarget.FingerPrint 
            $sessionOptions
 
            $session = New-Object WinSCP.Session

             
            try
            {
            
                # Connect
                $session.Open($sessionOptions)
                $session.FileExists($step.DestinationFolder)

                if (-Not ($session.FileExists($step.DestinationFolder)))
                {
                    $session.CreateDirectory($step.DestinationFolder)
                }
            

                # Make a list of files to be uploaded
                foreach ($path in $step.Paths) {
                    [array]$allFiles = @()

                    if (Test-Path $path -PathType Leaf) {
			            $allFiles += (Get-Item $path -Exclude $step.Exclude | ?{ !$_.PSIsContainer })
                    } else {
                        $allFiles += (Get-ChildItem $path -Exclude $step.Exclude -Recurse | ?{ !$_.PSIsContainer })
                    }
                                   
 
                    # Upload files
                    $transferOptions = New-Object WinSCP.TransferOptions
                    $transferOptions.TransferMode = [WinSCP.TransferMode]::Binary

                    foreach ($file in $allFiles) {
                        $file.FullName
                        $transferResult = $session.PutFiles($file.FullName, $step.DestinationFolder + "/" + $file.Name, $False, $transferOptions)

                        # Throw on any error
                        $transferResult.Check()

                        # Print results
                        foreach ($transfer in $transferResult.Transfers)
                        {
                            Write-Host ("Upload of {0} succeeded" -f $transfer.FileName)
                        }
                    }
 
                
                } # foreach path

            }            
            finally
            {
                # Disconnect, clean up
                $session.Dispose()
            }

        } # If FTP
  
	}


}

Function DoZip($steps) {
	foreach ($step in $steps) {
		Write-Output "Running ZIP step for destination [$($step.Destination)]..."
		
		# First we create folder if it doesn't exist
		$check = Test-Path -PathType Container $step.Destination
		if(($check -eq $false) -and ($step.Destination -notmatch "\.[\w]{1,3}$")){
    		$null = New-Item $step.Destination -type Directory
		}
		
		[array]$allFiles = @()
		$totalFileCount = 0
		
		# Get files
		foreach ($path in $step.Paths) {
			$allFiles += Get-Item $path -Exclude $step.Exclude
            if (Test-Path $path -PathType Leaf) {
			    $totalFileCount += (Get-Item $path -Exclude $step.Exclude | ?{ !$_.PSIsContainer } | Measure-Object).Count
            } else {
                $totalFileCount += (Get-ChildItem $path -Exclude $step.Exclude -Recurse | ?{ !$_.PSIsContainer } | Measure-Object).Count
            }
		}

        # Zip files on at a time, so we don't hit limit of path
        $counter = [pscustomobject] @{ Value = 0 }
        $groups = $allFiles | Group-Object -Property { [math]::Floor($counter.Value++ / 20) }

        foreach ($group in $groups) {
    		[array]$arguments = @("a", "-tzip", "-mmt" , $($step.Destination)) + $group.Group
    		$null = sz $arguments    
        }

		if ($LASTEXITCODE -gt 1) # Ignores warnings which use exit code 1.
		{
			throw "7zip failed with exit code $LASTEXITCODE"
		}	
				
		# Make sure we got what we wanted.
		if ($step.ExpectedDocuments -gt 0) {
			Write-Output "$($totalFileCount) files zipped. Expected $($step.ExpectedDocuments)."		
			if ($totalFileCount -ne $step.ExpectedDocuments) {
				Write-Error  "Mismatch when zipping $($step.Destination). Expected $($step.ExpectedDocuments) files, got $($totalFileCount)."
				WaitForFeedBack
				Exit
			}
		} else {
			Write-Output "$($allFiles.Count) files copied"		
		}		
	}

}

Function DoCopy($steps) {
	foreach ($step in $steps) {
		Write-Output "Running COPY step for destination [$($step.Destination)]..."
		
		# First we create folder if it doesn't exist
		$check = Test-Path -PathType Container $step.Destination
		if(($check -eq $false) -and ($step.Destination -notmatch "\.[\w]{1,3}$")){
    		$null = New-Item $step.Destination -type Directory
		}
		
		# See if we need to clean it up
		if ($step.CleanUp -eq $true) {
			Get-ChildItem $step.Destination -Recurse |  Remove-Item –Force -Recurse		
		}
		
		[array]$allFiles = @()
				
		# Copy files
		foreach ($path in $step.Paths) {
			Write-Output " FROM [$path]"
			[array]$result = Copy-Item $path $step.Destination -Exclude @([string]$step.Exclude,"*_bad.*") -passthru
			if ($result) {
				$allFiles += $result
			}
		}
				
		# Make sure we got what we wanted.
		if ($step.ExpectedDocuments -gt 0) {
			Write-Output "$($allFiles.Count) files copied. Expected $($step.ExpectedDocuments)."		
			if ($allFiles.Count -ne $step.ExpectedDocuments) {
				Write-Error "Mismatch when copying... From $path to $($step.Destination). Expected $($step.ExpectedDocuments) Copied $($allFiles.Count)"
				#WaitForFeedBack
				Exit
			}
		} else {
			Write-Output "$($allFiles.Count) files copied"		
		}		
	}

}

Function DoCleanup ($steps) {
    foreach ($step in $steps) {
        # First we create folder if it doesn't exist
		$check = Test-Path -PathType Container $step.Path
		if($check -eq $false){
    		$null = New-Item $step.Path -type Directory
		}
		
		if ($step.DeleteFolders -eq $true) {
			Get-ChildItem $step.Path -Force | Where-Object { Test-Path $_.FullName } | Remove-Item –Force -Recurse 		
		} else {
		       Get-ChildItem $step.Path -Recurse -Force | ?{ !$_.PSIsContainer }  | Remove-Item –Force -Recurse 
		}
    }
}


Function GetDateString($dateString) {
    if ($dateString -notmatch "^\d{4}(-(([Qq][0-9])|([0-9]{2}(-[0-9]{2})?)))?$") {
	    $dateString = ""
	    $dateString = Read-Host 'Enter Date (YYYY-XX)' 

	    if ($dateString -notmatch "^\d{4}(-(([Qq][0-9])|([0-9]{2})))?$") {
		    Write-Error "GetDateString: Invalid Date Format - The date format specified is in invalid format. Expected YYYY-XX"
		    WaitForFeedBack
		    Exit
	    }
    }
	return $dateString
}

Function GetQDateString($dateString) {
	$DateObj=[datetime]$dateString
	If(  ($DateObj -ge [datetime]($dateString.Substring(0,4) + "-03-01")) -and ($DateObj -le [datetime]($dateString.Substring(0,4) + "-03-31"))) {
		$QdateString = $dateString.Substring(0,4) + "-Q1"
	}
	If(  ($DateObj -ge [datetime]($dateString.Substring(0,4) + "-06-01")) -and ($DateObj -le [datetime]($dateString.Substring(0,4) + "-06-31"))) {
		$QdateString = $dateString.Substring(0,4) + "-Q2"
	}
	If(  ($DateObj -ge [datetime]($dateString.Substring(0,4) + "-09-01")) -and ($DateObj -le [datetime]($dateString.Substring(0,4) + "-09-31"))) {
		$QdateString = $dateString.Substring(0,4) + "-Q3"
	}
	If(  ($DateObj -ge [datetime]($dateString.Substring(0,4) + "-12-01")) -and ($DateObj -le [datetime]($dateString.Substring(0,4) + "-12-31"))) {
		$QdateString = $dateString.Substring(0,4) + "-Q4"
	}
	return $QdateString
}

Function GetMDateString($dateString) {
	If($dateString -match "Q1") {
		$MdateString = $dateString.Substring(0,4) + "-03"
	}
	If($dateString -match "Q2") {
		$MdateString = $dateString.Substring(0,4) + "-06"
	}
	If($dateString -match "Q3") {
		$MdateString = $dateString.Substring(0,4) + "-09"
	}
	If($dateString -match "Q4") {
		$MdateString = $dateString.Substring(0,4) + "-12"
	}
	return $MdateString
}

Function WaitForFeedBack() {
	Write-Output "Press any key to exit..."
    try {
	    $x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
    } catch [Exception] {

    }
}

Function PackagerStart($Name) {
    # Save current path
    $currentPath = Split-Path $script:MyInvocation.MyCommand.Path
    Push-Location $currentPath
       
    return $currentPath
}



Function PackagerEnd() {
    # now back to previous directory
    Pop-Location
}


Function DoChangeProperties ($steps) {
    foreach ($step in $steps) {
        Write-Output "Changing Properties for destination [$($step.Destination)]..."

        # First we create folder if it doesn't exist
		$check = Test-Path -PathType Leaf $step.Destination
		if($check -eq $false){
    		Write-Error "File [$($step.Destination)] does not exist."
			WaitForFeedBack
			Exit
		}

        # Read Existing Properties
        $arguments = @($step.Destination,"dump_data")
	    $data = (pdf $arguments) | out-string

        # Get
        $tempFile = [System.IO.Path]::GetTempFileName()
        
        foreach ($h in $step.Properties.Keys) {
            if ($data -imatch ([regex]"(?im)InfoKey: ${h}\r\nInfoValue: ([^\r\n]+)")) {
                $data = $data -replace ([regex]"(?im)InfoKey: ${h}\r\nInfoValue: ([^\r\n]+)"), "InfoKey: ${h}`r`nInfoValue: $($step.Properties.Item($h))"
            } else {
                $data = "InfoBegin`r`nInfoKey: ${h}`r`nInfoValue: $($step.Properties.Item($h))`r`n" + $data
            }
        }       

        $arguments = @($step.Destination,"update_info","-","output",$tempFile,"drop_xmp")
        $data | pdf $arguments

        if((Test-Path $tempFile) -eq $true){        
            Move-Item $tempFile -Destination $step.Destination -Force
        }
        

        # Remove temp template file
		if((Test-Path $tempFile) -eq $true){            
            Remove-Item $tempFile
        }
    }

}


Function DoChangeProperties_old ($steps) {
    foreach ($step in $steps) {
        Write-Output "Changing Properties for destination [$($step.Destination)]..."

        # First we create folder if it doesn't exist
		$check = Test-Path -PathType Leaf $step.Destination
		if($check -eq $false){
    		Write-Error "File [$($step.Destination)] does not exist."
			WaitForFeedBack
			Exit
		}

        # Read Existing Properties
        #$arguments = @($step.Destination,"dump_data")
	    #$data = (pdf $arguments) | out-string

        # Get
        $tempFile = [System.IO.Path]::GetTempFileName()

        Write-Output "Using temp file $tempFile"

        "[Info]" | Out-File $tempFile -Encoding ASCII
        
        foreach ($h in $step.Properties.Keys) {
            "${h}=""$($step.Properties.Item($h))""" | Out-File $tempFile -Encoding ASCII -Append
            
        }       
        
        [array]$arguments = @("$($step.Destination)","-tf","$tempFile","-q","-tm","i","-X","1")

        pdfmeta $arguments    
        if ($LASTEXITCODE -gt 0) # Ignores warnings which use exit code 1.
        {
	        throw "BeCyPDFMetaEdit failed with exit code $LASTEXITCODE"
        }


        
        # Remove temp template file
		if((Test-Path $tempFile) -eq $false){            
            Remove-Item $tempFile
        }
    }

}


function DoUnzip($steps)	{
$dir = PackagerStart
    foreach ($step in $steps)	{
		Write-Output "Running unZIP step for destination [$($step.Destination)]..."
		
		# First we create folder if it doesn't exist
		$check = Test-Path -PathType Container $step.Destination
		if(($check -eq $false) -and ($step.Destination -notmatch "\.[\w]{1,3}$")){
    		$null = New-Item $step.Destination -type Directory
		}
		
		# See if we need to clean it up
		if ($step.CleanUp -eq $true) {
			Get-ChildItem $step.Destination -Recurse |  Remove-Item –Force -Recurse		
		}
		
		[array]$allFiles = @()
		
		#-o flag needs to be joined to $step.Destination variable
		$outputOption = $step.Destination
        $outputOption = "-o$outputOption"
		# unzip already!
		[array]$arguments = @("e", "-tzip", $step.Path , $outputOption,"-y")
    	sz $arguments    
		$allFiles = ( Get-ChildItem $step.Destination | Measure-Object ).Count
		
		# Make sure we got what we wanted.
		if ($step.ExpectedDocuments -gt 0) {
			Write-Output "$allFiles files unzipped. Expected $($step.ExpectedDocuments)."		
			if ($allFiles -ne $step.ExpectedDocuments) {
				Write-Error "Mismatch when unzipping... From $path to $($step.Destination). Expected $($step.ExpectedDocuments) unzipped $allFiles"
				#WaitForFeedBack
				Exit
			}
		} else {
			Write-Output "$allFiles files unzipped"		
		}
	}
}